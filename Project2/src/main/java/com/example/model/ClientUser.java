package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Client_User")
public class ClientUser {
	
	@Id
	@Column(name="client_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int clientId;
	
	
	@Column(name="first_name", nullable=false)
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="address")
	private String address;
	
	@Column(name="age")
	private int age;
	
	@Column(name="email_address")
	private String emailAddress;
	
	@Column(name="contact_Number")
	private String contactNumber;
	
    @Lob
    @Column(name = "photo", columnDefinition="BLOB")
    private byte[] photo;
	
	@OneToMany(mappedBy="cHolder", fetch=FetchType.LAZY)
	private List<Account> accList = new ArrayList<>();

	public ClientUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClientUser(int clientId, String firstName, String lastName, String address, int age, String emailAddress,
			String contactNumber, List<Account> accList) {
		super();
		this.clientId = clientId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.age = age;
		this.emailAddress = emailAddress;
		this.contactNumber = contactNumber;
		this.accList = accList;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public List<Account> getAccList() {
		return accList;
	}

	public void setAccList(List<Account> accList) {
		this.accList = accList;
	}

	public int getClientId() {
		return clientId;
	}

	@Override
	public String toString() {
		return "ClientUser [clientId=" + clientId + ", firstName=" + firstName + ", lastName=" + lastName + ", address="
				+ address + ", age=" + age + ", emailAddress=" + emailAddress + ", contactNumber=" + contactNumber
				+ ", accList=" + accList + "]";
	}

	
    
	
}
