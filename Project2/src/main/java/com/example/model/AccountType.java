package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Account_Type")
public class AccountType {
	@Id
    @Column(name = "type_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int TypeId;
	
	@Column(name="type_name")
	private String typeName;
	
	@OneToMany(mappedBy="tHolder", fetch=FetchType.LAZY)
	private List<Account> accList = new ArrayList<>();

	public AccountType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountType(int typeId, String type, List<Account> accList) {
		super();
		TypeId = typeId;
		this.typeName = type;
		this.accList = accList;
	}

	public String getType() {
		return typeName;
	}

	public void setType(String type) {
		this.typeName = type;
	}

	public List<Account> getAccList() {
		return accList;
	}

	public void setAccList(List<Account> accList) {
		this.accList = accList;
	}

	public int getTypeId() {
		return TypeId;
	}

	@Override
	public String toString() {
		return "AccountType [TypeId=" + TypeId + ", type=" + typeName + ", accList=" + accList + "]";
	}
	
	
}
